// ==========================================================
// Project: XSpotify
// 
// Component: XSpotify.dll
//
// Purpose: Experimental bitrate stuff
//          
// Initial author: Meik Wochnik
//
// Started: 15.10.2019
// ==========================================================

#pragma once
#include "..\\include\BaseInclude.hpp"
int checkValue;

signed int SetBitrate_hk(int a1, int a2, int a3, int a4, int a5)
{
	checkValue = a1;
	return 320000;
}

signed int SetBitrate_hk2(int a1)
{
	return 320000;
}

void __declspec(naked) ExecBitrate_stub(int a1)
{
	__asm
	{
		cmp     ecx, 9
		ja      defaultCase
		jmp     highBitRateCase

		defaultCase:
		push    0E44DC5h
		retn

		highBitRateCase:
		push    0E44D91h
		retn
	}
}

int jamoyn(int a1)
{
	return 3;
}

void __declspec(naked) Codec(int a1, int a2, int a3)
{
	__asm
	{
		cmp     eax, 96000
		jz      OGG320

		cmp     eax, 160000
		jz      OGG320

		cmp     eax, 320000
		jz      OGG320

		OGG320:
		push    0C46862h
		retn
	}
}

void __declspec(naked) kek1_stub(int a1, int a2, int a3)
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		push    1228400h
		push    12CEDACh
		push    0E18D0Dh
		retn
	}
}

void __declspec(naked) kek2_stub(int a1, int a2, int a3)
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		push    1228418h
		push    12CEDCCh
		push    0E18D3Dh
		retn
	}
}

void kek1(int a1, int a2, int a3)
{
	std::cout << "SAVED: " << checkValue << std::endl;
	std::cout << "CV: " << a1 << std::endl;

	if (checkValue == 0)
	{
		std::cout << "NULL!" << std::endl;
	}
	else
	{
		a1 = checkValue;
	}

	std::cout << "SAVED: " << checkValue << std::endl;
	std::cout << "CV FINAL: " << a1 << std::endl;
	kek1_stub(a1, a2, a3);
}

void kek2(int a1, int a2, int a3)
{
	if (checkValue == 0)
	{
		std::cout << "NULL!" << std::endl;
	}
	else
	{
		a1 = checkValue;
	}
	
	kek2_stub(a1, a2, a3);
}

void XPatchBitrate()
{
	//Hook::InstallJmp((void*)0xE18B80, SetBitrate_hk); //not returning bitrate after patch
	//Hook::InstallJmp((void*)0xE18E60, SetBitrate_hk2);
	//Hook::InstallJmp((void*)0xE44D61, ExecBitrate_stub);
	//Hook::InstallJmp((void*)0xE18D00, kek1);
	//Hook::InstallJmp((void*)0xE18D30, kek2);
	//Hook::InstallJmp((void*)0xE18D60, jamoyn);
	//Hook::InstallJmp((void*)0xC46845, Codec);
}